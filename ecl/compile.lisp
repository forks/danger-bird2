(require 'cmp)

(defun compile-clbird (&optional (source "clbird.lisp") (afile "clbird.a"))
  (let* ((src-length (length source))
	 ;; HACK(eta): what the heck
	 (c::+static-library-format+ "~a.a")
	 (cursed-obj-file (concatenate 'string
				       (subseq source 0 (- src-length 4))
				       "o")))
    (compile-file source :system-p t)
    (c::build-static-library afile
			     :lisp-files (list cursed-obj-file)
			     :init-name "clbird_native_lisp_init")
    (quit)))
