(defpackage :bird-impl
  (:use))

(defpackage :bird-filter
  (:use))

(defpackage :bird
  (:nicknames :b)
  (:use :cl)
  (:export :version :deffilter))

(in-package :bird)

(defclass bird-routing-table ()
  ((ptr
    :initarg :ptr)))

(defmethod print-object ((obj bird-routing-table) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (ptr) obj
      (format stream "\"~A\"" (bird-impl::table-name ptr)))))

(defun make-routing-table (ptr)
  "Make a BIRD-ROUTING-TABLE from a foreign PTR."
  (make-instance 'bird-routing-table
                 :ptr ptr))

(defclass bird-val ()
  ((ptr
    :initarg :ptr)
   (ty
    :initarg :ty)))

(defun val-format (val)
  "Formats VAL, a BIRD-VAL, to a string."
  (bird-impl::format-val (slot-value val 'ptr)))

(defmethod print-object ((obj bird-val) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (ptr ty) obj
      (format stream "~A \"~A\" ~A" (bird-impl::val-type-name ptr) (bird-impl::format-val ptr) ptr))))

(defclass bird-route ()
  ((ptr
    :initarg :ptr)))

(defun make-route (ptr)
  "Make a BIRD-ROUTE from a foreign PTR."
  (make-instance 'bird-route
                 :ptr ptr))

(defclass val-pair (bird-val) ())

(defmethod make-load-form ((self val-pair) &optional env)
  (declare (ignore env))
  (let ((pair (bird-impl::unmake-pair (slot-value self 'ptr))))
    `(bird::make-pair ,(car pair) ,(cdr pair))))

(defclass val-quad (bird-val) ())

(defmethod make-load-form ((self val-quad) &optional env)
  (declare (ignore env))
  (let ((quad (bird-impl::unmake-quad (slot-value self 'ptr))))
    `(bird::make-quad-int ,quad)))

(defclass val-ip (bird-val) ())

(defclass val-net (bird-val) ())

(defmethod make-load-form ((self val-net) &optional env)
  (declare (ignore env))
  (let* ((ret (bird-impl::uncidr4 (slot-value self 'ptr)))
         (addr (car ret))
         (o1 (ash addr -24))
         (o2 (logand (ash addr -16) #xFF))
         (o3 (logand (ash addr -8) #xFF))
         (o4 (logand addr #xFF))
         (pxlen (cdr ret)))
    `(bird::cidr4 ,o1 ,o2 ,o3 ,o4 ,pxlen)))

(defun bird-type-id-to-class (id)
  "Returns a symbol for the given BIRD type ID. See filter/data.h."
  (declare (fixnum id))
  (case id
    (#x12 'val-pair)
    (#x13 'val-quad)
    (#x20 'val-ip)
    (#x21 'val-net)
    (t 'bird-val)))

(defun make-bird-val (ptr)
  "Make a BIRD-VAL from a foreign PTR."
  (let* ((ty (bird-impl::val-type-id ptr))
         (cls (bird-type-id-to-class ty))
         (plain (make-instance 'bird-val
                               :ptr ptr
                               :ty ty)))
    (change-class plain cls)))

(defun maybe-make-bird-val (obj)
  "If OBJ is a foreign pointer, run MAKE-BIRD-VAL on it; otherwise, return OBJ."
  (if (typep obj 'si::foreign-data)
      (make-bird-val obj)
      obj))

(defun make-pair (int1 int2)
  "Make a VAL-PAIR from a cons CELL."
  (make-bird-val (bird-impl::make-pair int1 int2)))

(defun make-quad-int (int)
  "Make a VAL-QUAD from a raw integer."
  (make-bird-val (bird-impl::make-quad int)))
(defun cidr4 (o1 o2 o3 o4 prefix-len)
  "Make a VAL-NET from 4 octets and a prefix length (i.e. o1.o2.o3.o4/prefix-len)"
  (make-bird-val (bird-impl::cidr4 o1 o2 o3 o4 prefix-len)))

(defparameter +left-sq-bracket+ #\[)
(defparameter +right-sq-bracket+ #\])
(defparameter +full-stop+ #\.)
(defparameter +slash+ #\/)
(defparameter +comma+ #\,)
(defparameter +left-paren+ #\()
(defparameter +right-paren+ #\))

(defun read-cidr (state &optional (stream *standard-input*))
  "Read a dotted quad or CIDR thing from STREAM."
  (flet ((peek () (peek-char t stream t nil t))
         (discard () (read-char stream t nil t)))
    (let ((next (peek))
          obj)
      (if (char= next +right-sq-bracket+)
          (progn
            (discard)
            state)
          (progn
            (setf obj (read stream t nil t))
            (setf next (peek))
            (cond
              ((char= next +full-stop+) (discard))
              ((char= next +comma+) (discard))
              ((char= next +slash+) (discard))
              ((char= next +right-sq-bracket+) nil)
              (t (error "Unexpected ~A in CIDR notation" next)))
            (unless (typep obj '(fixnum 0 256))
              (error "Invalid object ~A in CIDR notation" obj))
            (read-cidr (cons obj state) stream))))))

(defun read-separator (stream char)
  (declare (ignore stream char))
  (error "Separator character shouldn't ever be read"))

(defun sharp-left-bracket (stream char n)
  "Sharpsign reader macro for #["
  (declare (ignore char n))
  (let ((*readtable* (copy-readtable)))
    (set-macro-character +full-stop+ #'read-separator)
    (set-macro-character +slash+ #'read-separator)
    (set-macro-character +left-sq-bracket+ #'read-separator)
    (set-macro-character +right-sq-bracket+ #'read-separator)
    (let ((cidr (nreverse (read-cidr nil stream))))
      (cond
        ((eql (length cidr) 2)
         (apply #'bird::make-pair cidr))
        ((eql (length cidr) 4)
         (bird::make-quad-int
           (+
             (ash (elt cidr 0) 24)
             (ash (elt cidr 1) 16)
             (ash (elt cidr 2) 8)
             (elt cidr 3))))
        ((eql (length cidr) 5)
         (apply #'bird::cidr4 cidr))
        (t
         (error "Invalid CIDR: ~A" cidr))))))

(set-dispatch-macro-character #\# #\[ #'sharp-left-bracket)

(defun bird-equal (val1 val2)
  "Returns T if VAL1 is the same thing as VAL2 (both BIRD-VALs)."
  (check-type val1 bird-val)
  (check-type val2 bird-val)
  (bird-impl::val-same (slot-value val1 'ptr)
                       (slot-value val2 'ptr)))

(defun bird-compare (val1 val2)
  "Returns -1 (<), 0 (=), or 1 (>) as a comparison result for VAL1 and VAL2."
  (check-type val1 bird-val)
  (check-type val2 bird-val)
  (bird-impl::val-compare (slot-value val1 'ptr)
                          (slot-value val2 'ptr)))

(defun val< (val1 val2) (eql (bird-compare val1 val2) -1))
(defun val> (val1 val2) (eql (bird-compare val1 val2) 1))
(defun val= (val1 val2) (bird-equal val1 val2))
(defun val>= (val1 val2) (member (bird-compare val1 val2) '(0 1)))
(defun val<= (val1 val2) (member (bird-compare val1 val2) '(0 -1)))

(defun ~ (val1 val2)
  "Returns T if VAL1 is in the range VAL2 (both BIRD-VALs)."
  (when (and val1 val2)
    (check-type val1 bird-val)
    (check-type val2 bird-val)
    (bird-impl::val-in-range (slot-value val1 'ptr)
                             (slot-value val2 'ptr))))

(defun routing-tables ()
  "Get all of the BIRD routing tables."
  (mapcar #'make-routing-table (bird-impl::routing-tables)))

(defun table-routes (table)
  "Get the routes for the routing table TABLE."
  (check-type table bird-routing-table)
  (mapcar #'make-route (bird-impl::table-routes (slot-value table 'ptr))))

(defun table-routes-for (table prefix)
  "Get the routes for the prefix PREFIX in the routing table TABLE."
  (check-type table bird-routing-table)
  (check-type prefix val-net)
  (mapcar #'make-route (bird-impl::table-routes-net
                        (slot-value table 'ptr)
                        (slot-value prefix 'ptr))))

(defun for-each-route (table function)
  "Run FUNCTION once for each route in TABLE."
  (check-type table bird-routing-table)
  (let ((route nil))
    (bird-impl::for-table-routes
     (slot-value table 'ptr)
     (lambda (rte)
       (let ((obj-rte (if route
                          (progn
                            (setf (slot-value route 'ptr) rte)
                            route)
                          (setf route (make-route rte)))))
         (funcall function obj-rte))))))

(defun route-attr (route attr)
  "Get a route attribute."
  (check-type route bird-route)
  (maybe-make-bird-val (bird-impl::get-val attr (slot-value route 'ptr))))

(defmethod print-object ((obj bird-route) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (ptr ty) obj
      (format stream "for ~A proto ~A (~A)"
              (val-format (route-attr obj :net))
              (route-attr obj :proto)
              ptr))))

(defun router-id ()
  "Returns the running BIRD router ID, as a VAL-QUAD."
  (make-quad-int (bird-impl::router-id)))

(defun version ()
  "Returns the running BIRD version, as a string."
  (bird-impl::version))

(defun hostname ()
  "Returns the hostname BIRD is running on, as a string."
  (bird-impl::hostname))

(defun ea-code (proto id)
  (logior (ash proto 8) id))

(defparameter +protocol-none+ 0)
(defparameter +protocol-babel+ 1)
(defparameter +protocol-bfd+ 2)
(defparameter +protocol-bgp+ 3)
(defparameter +protocol-device+ 4)
(defparameter +protocol-direct+ 5)
(defparameter +protocol-kernel+ 6)
(defparameter +protocol-ospf+ 7)
(defparameter +protocol-mrt+ 8)
(defparameter +protocol-perf+ 9)
(defparameter +protocol-pipe+ 10)
(defparameter +protocol-radv+ 11)
(defparameter +protocol-rip+ 12)
(defparameter +protocol-rpki+ 13)
(defparameter +protocol-static+ 14)
;; BGP protocol attributes
(defparameter +extended-attrs+
  `((:bgp-origin ,(ea-code +protocol-bgp+ #x01))
    (:bgp-as-path ,(ea-code +protocol-bgp+ #x02))
    (:bgp-next-hop ,(ea-code +protocol-bgp+ #x03))
    (:bgp-multi-exit-disc ,(ea-code +protocol-bgp+ #x04))
    (:bgp-local-pref ,(ea-code +protocol-bgp+ #x05))
    (:bgp-atomic-aggr ,(ea-code +protocol-bgp+ #x06))
    (:bgp-aggregator ,(ea-code +protocol-bgp+ #x07))
    (:bgp-community ,(ea-code +protocol-bgp+ #x08))
    (:bgp-originator-id ,(ea-code +protocol-bgp+ #x09))
    (:bgp-cluster-list ,(ea-code +protocol-bgp+ #x0a))
    (:bgp-mp-reach-nlri ,(ea-code +protocol-bgp+ #x0e))
    (:bgp-mp-unreach-nlri ,(ea-code +protocol-bgp+ #x0f))
    (:bgp-ext-community ,(ea-code +protocol-bgp+ #x10))
    (:bgp-as4-path ,(ea-code +protocol-bgp+ #x11))
    (:bgp-as4-aggregator ,(ea-code +protocol-bgp+ #x12))
    (:bgp-aigp ,(ea-code +protocol-bgp+ #x1a))
    (:bgp-large-community ,(ea-code +protocol-bgp+ #x20))
    (:gen-igp-metric ,(ea-code +protocol-none+ 0))
    (:ospf-metric1 ,(ea-code +protocol-ospf+ 0))
    (:ospf-metric2 ,(ea-code +protocol-ospf+ 1))
    (:ospf-tag ,(ea-code +protocol-ospf+ 2))
    (:ospf-router-id ,(ea-code +protocol-ospf+ 3))
    (:kernel-source ,(ea-code +protocol-kernel+ 0))
    (:kernel-metric ,(ea-code +protocol-kernel+ 1))
    (:kernel-prefsrc ,(ea-code +protocol-kernel+ #x10))
    (:kernel-realm ,(ea-code +protocol-kernel+ #x11))
    (:kernel-scope ,(ea-code +protocol-kernel+ #x12))
    (:kernel-metrics ,(ea-code +protocol-kernel+ #x20))
    (:kernel-lock ,(ea-code +protocol-kernel+ #x21))
    (:kernel-mtu ,(ea-code +protocol-kernel+ #x22))
    (:kernel-window ,(ea-code +protocol-kernel+ #x23))
    (:kernel-rtt ,(ea-code +protocol-kernel+ #x24))
    (:kernel-rttvar ,(ea-code +protocol-kernel+ #x25))
    (:kernel-sstresh ,(ea-code +protocol-kernel+ #x26))
    (:kernel-cwnd ,(ea-code +protocol-kernel+ #x27))
    (:kernel-advmss ,(ea-code +protocol-kernel+ #x28))
    (:kernel-reordering ,(ea-code +protocol-kernel+ #x29))
    (:kernel-hoplimit ,(ea-code +protocol-kernel+ #x2a))
    (:kernel-initcwnd ,(ea-code +protocol-kernel+ #x2b))
    (:kernel-features ,(ea-code +protocol-kernel+ #x2c))
    (:kernel-rto-min ,(ea-code +protocol-kernel+ #x2d))
    (:kernel-initrwnd ,(ea-code +protocol-kernel+ #x2e))
    (:kernel-quickack ,(ea-code +protocol-kernel+ #x2f))
    (:rip-metric ,(ea-code +protocol-rip+ 0))
    (:rip-tag ,(ea-code +protocol-rip+ 1))
    (:radv-preference ,(ea-code +protocol-radv+ 0))
    (:radv-lifetime ,(ea-code +protocol-radv+ 1))
    (:babel-metric ,(ea-code +protocol-babel+ 0))
    (:babel-router-id ,(ea-code +protocol-babel+ 1))))

(defun ea-hash-table (&optional reverse)
  (let ((ret (make-hash-table)))
    (loop
      for (kw code) in +extended-attrs+
      do (let ((key (if reverse kw code))
               (value (if reverse code kw)))
           (setf (gethash key ret) value)))
    ret))

(defparameter +ea-code-to-keyword+ (ea-hash-table))
(defparameter +ea-keyword-to-code+ (ea-hash-table t))

(defun route-eattrs (route)
  "Get the extended attributes for ROUTE."
  (check-type route bird-route)
  (loop
    for (id . val) in (bird-impl::route-eattrs (slot-value route 'ptr))
    collect (cons
             (gethash id +ea-code-to-keyword+ :unknown)
             (maybe-make-bird-val val))))

(defun route-eattr (route id)
  "Get the extended attribute with id ID for ROUTE."
  (check-type route bird-route)
  (maybe-make-bird-val (bird-impl::route-eattr (slot-value route 'ptr) id)))

(defparameter *route* nil)

(defun sharp-exclamation (stream char n)
  (declare (ignore char n))
  (let ((sym (read stream t nil t))
        (eattr-id))
    (unless (typep sym 'symbol)
      (error "Invalid #! target ~A" sym))
    (setf sym (intern (symbol-name sym) :keyword))
    (unless (setf eattr-id (gethash sym +ea-keyword-to-code+))
      (error "Routes do not have an extended attribute ~A (maybe try #? syntax?)" sym))
    `(bird::route-eattr bird::*route* ,eattr-id)))

(set-dispatch-macro-character #\# #\! #'sharp-exclamation)

(defun sharp-question (stream char n)
  (declare (ignore char n))
  (let ((sym (read stream t nil t)))
    (unless (typep sym 'symbol)
      (error "Invalid #? target ~A" sym))
    (setf sym (intern (symbol-name sym) :keyword))
    (unless (member sym '(:from :gw :net :proto :source :scope
                          :dest :ifname :ifindex :weight))
      (error "Routes do not have an attribute ~A (maybe try #! syntax?)" sym))
    `(bird::route-attr bird::*route* ,sym)))

(set-dispatch-macro-character #\# #\? #'sharp-question)

(defmacro deffilter (name (route-name) &body body)
  "Define a BIRD filter called NAME, with syntax similar to DEFUN. The route passed in as ROUTE-NAME is a BIRD-ROUTE object."
  (let ((name-sym (intern (symbol-name name) 'bird-filter))
        (rte-csym (gensym))
        (rte-sym (gensym)))
    `(let ((,rte-csym nil))
       (defun ,name-sym (,rte-sym)
         (let* ((,route-name (if ,rte-csym
                                 (progn
                                   (setf (slot-value ,rte-csym 'bird::ptr) ,rte-sym)
                                   ,rte-csym)
                                 (setf ,rte-csym (bird::make-route ,rte-sym))))
                (bird::*route* ,route-name))
           (block nil
             ,@body))))))
