/*
 *  Embeddable Common Lisp support for BIRD
 *
 *  (c) 2021 eta <hi@theta.eu.org>
 */

#ifndef _BIRD_CLDEFS_H_
#define _BIRD_CLDEFS_H_

#include <ecl/ecl.h>

#define ecls(x) ecl_make_simple_base_string(x,-1)
#define eclk(x) ecl_make_keyword(x)
#define eclcs(x) ecl_make_constant_base_string(x,-1)

// from libclbird.a
extern void clbird_native_lisp_init(cl_object);

void *clbird_alloc(size_t);

cl_object clbird_version(void);
cl_object clbird_hostname(void);
cl_object clbird_router_id(void);
cl_object clbird_nullptr(void);
cl_object clbird_format_val(cl_object obj);
cl_object clbird_get_val(cl_object which, cl_object rte_obj);
cl_object clbird_get_routing_tables(void);
cl_object clbird_get_table_name(cl_object obj);
cl_object clbird_val_type_name(cl_object val_obj);
cl_object clbird_val_type_id(cl_object val_obj);
cl_object clbird_get_table_routes_all(cl_object table_obj);
cl_object clbird_get_table_routes_net(cl_object table_obj, cl_object net_obj);
cl_object clbird_dotquad(cl_object o1, cl_object o2, cl_object o3, cl_object o4);
cl_object clbird_cidr4(cl_object o1, cl_object o2, cl_object o3, cl_object o4, cl_object len);
cl_object clbird_val_compare(cl_object val1_obj, cl_object val2_obj);
cl_object clbird_val_same(cl_object val1_obj, cl_object val2_obj);
cl_object clbird_val_in_range(cl_object val1_obj, cl_object val2_obj);
cl_object clbird_make_val(cl_object from);
cl_object clbird_make_quad(cl_object from);
cl_object ea_to_object(eattr *e);
cl_object clbird_get_eattrs(cl_object rte_obj);
void init_ecl_functions(void);
void clbird_init(int, char**);
char* ecl_get_string_for(cl_object obj);
void clbird_load(char*);
void clbird_load_and_byte_compile(char*);

#endif
