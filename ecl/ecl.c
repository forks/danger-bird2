/*
 *  Embeddable Common Lisp support for BIRD
 *
 *  (c) 2021 eta <hi@theta.eu.org>
 */

#include <ecl/ecl.h>

#include "nest/bird.h"
#include "lib/lists.h"
#include "lib/resource.h"
#include "lib/socket.h"
#include "lib/event.h"
#include "lib/timer.h"
#include "lib/string.h"
#include "nest/route.h"
#include "nest/protocol.h"
#include "nest/iface.h"
#include "nest/cli.h"
#include "nest/locks.h"
#include "conf/conf.h"
#include "filter/filter.h"
#include "filter/data.h"
#include "cldefs.h"

void* clbird_val_alloc_space = NULL;

void *
clbird_alloc(size_t amount)
{
	if (clbird_val_alloc_space == NULL) {
		return ecl_alloc(amount);
	}
	else {
		// this isn't cursed at all!
		void* ptr = clbird_val_alloc_space;
		clbird_val_alloc_space += amount;
		return ptr;
	}
}

cl_object
clbird_version()
{
	return eclcs(BIRD_VERSION);
}
cl_object
clbird_hostname()
{
	return ecls(config->hostname);
}

cl_object
clbird_router_id()
{
	return ecl_make_fixnum(config->router_id);
}

cl_object
clbird_nullptr()
{
	return ecl_make_pointer(NULL);
}
cl_object
clbird_format_val(cl_object obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(obj);
	buffer buf;
	LOG_BUFFER_INIT(buf);
	val_format(val, &buf);
	return ecls(buf.start);

}
cl_object
clbird_get_val(cl_object which, cl_object rte_obj)
{
	struct rte *rte = ecl_foreign_data_pointer_safe(rte_obj);
	struct f_val *res = clbird_alloc(sizeof(struct f_val));
	struct rta *rta = rte->attrs;
	cl_object ret = NULL;

	if (ecl_eql(which, eclk("FROM"))) {
		res->type = T_IP;
		res->val.ip = rta->from;
	}
	else if (ecl_eql(which, eclk("GW"))) {
		res->type = T_IP;
		res->val.ip = rta->nh.gw;
	}
	else if (ecl_eql(which, eclk("NET"))) {
		res->type = T_NET;
		res->val.net = rte->net->n.addr;
	}
	else if (ecl_eql(which, eclk("PROTO"))) {
		ret = ecls(rta->src->proto->name);
	}
	else if (ecl_eql(which, eclk("SOURCE"))) {
		res->type = T_ENUM_RTS;
		res->val.i = rta->source;
	}
	else if (ecl_eql(which, eclk("SCOPE"))) {
		res->type = T_ENUM_SCOPE;
		res->val.i = rta->scope;
	}
	else if (ecl_eql(which, eclk("DEST"))) {
		res->type = T_ENUM_RTD;
		res->val.i = rta->dest;
	}
	else if (ecl_eql(which, eclk("IFNAME"))) {
		ret = rta->nh.iface ? ecls(rta->nh.iface->name) : ECL_NIL;
	}
	else if (ecl_eql(which, eclk("IFINDEX"))) {
		ret = rta->nh.iface ? ecl_make_fixnum(rta->nh.iface->index) : ECL_NIL;
	}
	else if (ecl_eql(which, eclk("WEIGHT"))) {
		ret = ecl_make_fixnum(rta->nh.weight + 1);
	}
	else {
		FEerror("Unknown route attribute ~A.", 1, which);
	}
	if (ret != NULL) {
		return ret;
	}
	else {
		return ecl_make_pointer(res);
	}
}
cl_object
clbird_get_routing_tables()
{
	cl_object ret = ECL_NIL;
	for (int i = 1; i < NET_MAX; i++) {
		if (config->def_tables[i]) {
			ret = ecl_cons(ecl_make_pointer(config->def_tables[i]->table), ret);
		}
	}
	return ret;
}
cl_object
clbird_get_table_name(cl_object obj)
{
	rtable *table = ecl_foreign_data_pointer_safe(obj);
	return ecls(table->name);
}
cl_object
clbird_val_type_name(cl_object val_obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(val_obj);
	return eclcs(f_type_name(val->type));
}
cl_object
clbird_val_type_id(cl_object val_obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(val_obj);
	return ecl_make_fixnum(val->type);
}
cl_object
clbird_get_table_routes_all(cl_object table_obj)
{
	rtable *table = ecl_foreign_data_pointer_safe(table_obj);
	cl_object ret = ECL_NIL;
	FIB_WALK(&table->fib, net, n)
	{
		for (rte *e = n->routes; e; e = e->next) {
			ret = ecl_cons(ecl_make_pointer(e), ret);
		}
	}
	FIB_WALK_END;
	return ret;
}
cl_object
clbird_for_table_routes_all(cl_object table_obj, cl_object func_obj)
{
	rtable *table = ecl_foreign_data_pointer_safe(table_obj);
	cl_object v = eclk("VOID");
	FIB_WALK(&table->fib, net, n)
	{
		for (rte *e = n->routes; e; e = e->next) {
			union cl_lispunion rte_obj;
			rte_obj.d.t = t_foreign;
			rte_obj.foreign.tag = v;
			rte_obj.foreign.size = 0;
			rte_obj.foreign.data = e;
			cl_funcall(2, func_obj, &rte_obj);
		}
	}
	FIB_WALK_END;
	return ECL_NIL;
}
cl_object
clbird_get_table_routes_net(cl_object table_obj, cl_object net_obj)
{
	rtable *table = ecl_foreign_data_pointer_safe(table_obj);
	struct f_val *net_val = ecl_foreign_data_pointer_safe(net_obj);
	if (net_val->type != T_NET) {
		FEerror("TABLE-ROUTES-NET needs a prefix, not a ~A", 1, clbird_val_type_name(net_obj));
	}
	const net_addr* na = net_val->val.net;
	net* net = net_route(table, na);
	if (!net) {
		return ECL_NIL;
	}
	cl_object ret = ECL_NIL;
	for (rte *e = net->routes; e; e = e->next) {
		ret = ecl_cons(ecl_make_pointer(e), ret);
	}
	return ret;
}
cl_object
clbird_cidr4(cl_object o1, cl_object o2, cl_object o3, cl_object o4, cl_object len)
{
	cl_fixnum ret = 0;
	ret |= ecl_to_fix(o1) << 24;
	ret |= ecl_to_fix(o2) << 16;
	ret |= ecl_to_fix(o3) << 8;
	ret |= ecl_to_fix(o4);
	net_addr *na = clbird_alloc(sizeof(net_addr));
	net_fill_ip4(na, _MI4(ret), ecl_to_fix(len));
	struct f_val *res = clbird_alloc(sizeof(struct f_val));
	res->type = T_NET;
	res->val.net = na;
	return ecl_make_pointer(res);
}
cl_object
clbird_uncidr4(cl_object net_obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(net_obj);
	if (val->type != T_NET) {
		FEerror("Can't UNCIDR4 a ~A", 1, eclcs(f_type_name(val->type)));
	}
	if (val->val.net->type != NET_IP4) {
		FEerror("Can only UNCIDR4 an IPv4 address.", 0);
	}
	net_addr_ip4 *na = (net_addr_ip4 *) val->val.net;
	cl_fixnum pxlen = na->pxlen;
	cl_fixnum addr = _I(na->prefix);
	return ecl_cons(ecl_make_fixnum(addr), ecl_make_fixnum(pxlen));
}
cl_object
clbird_make_pair(cl_object p1, cl_object p2)
{
	struct f_val *res = clbird_alloc(sizeof(struct f_val));
	res->type = T_PAIR;
	res->val.i = ecl_to_fix(p1) << 16 | ecl_to_fix(p2);
	return ecl_make_pointer(res);
}
cl_object
clbird_unmake_pair(cl_object pair_obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(pair_obj);
	if (val->type != T_PAIR) {
		FEerror("Can't UNMAKE-PAIR a ~A", 1, eclcs(f_type_name(val->type)));
	}
	cl_fixnum pair = val->val.i;
	return ecl_cons(ecl_make_fixnum(pair >> 16), ecl_make_fixnum(pair & 0xFFFF));
}
cl_object
clbird_make_quad(cl_object quadint)
{
	struct f_val *res = clbird_alloc(sizeof(struct f_val));
	res->type = T_QUAD;
	res->val.i = ecl_to_fix(quadint);
	return ecl_make_pointer(res);
}
cl_object
clbird_unmake_quad(cl_object val1_obj)
{
	struct f_val *val = ecl_foreign_data_pointer_safe(val1_obj);
	if (val->type != T_QUAD) {
		FEerror("Can't UNMAKE-QUAD a ~A", 1, eclcs(f_type_name(val->type)));
	}
	cl_fixnum quad = val->val.i;
	return ecl_make_fixnum(quad);
}
cl_object
clbird_val_compare(cl_object val1_obj, cl_object val2_obj)
{
	struct f_val *val1 = ecl_foreign_data_pointer_safe(val1_obj);
	struct f_val *val2 = ecl_foreign_data_pointer_safe(val2_obj);
	cl_fixnum ret = val_compare(val1, val2);
	if (ret == F_CMP_ERROR) {
		FEerror("Comparison of raw BIRD values ~A (~A) and ~A (~A) failed.", 4, val1_obj, eclcs(f_type_name(val1->type)), val2_obj, eclcs(f_type_name(val2->type)));
	}
	return ecl_make_fixnum(ret);
}
cl_object
clbird_val_same(cl_object val1_obj, cl_object val2_obj)
{
	struct f_val *val1 = ecl_foreign_data_pointer_safe(val1_obj);
	struct f_val *val2 = ecl_foreign_data_pointer_safe(val2_obj);
	return val_same(val1, val2) ? ECL_T : ECL_NIL;
}
cl_object
clbird_val_in_range(cl_object val1_obj, cl_object val2_obj)
{
	struct f_val *val1 = ecl_foreign_data_pointer_safe(val1_obj);
	struct f_val *val2 = ecl_foreign_data_pointer_safe(val2_obj);
	cl_fixnum ret = val_in_range(val1, val2);
	if (ret == F_CMP_ERROR) {
		FEerror("Comparison of raw BIRD values ~A (~A) and ~A (~A) failed.", 4, val1_obj, eclcs(f_type_name(val1->type)), val2_obj, eclcs(f_type_name(val2->type)));
	}
	return ret ? ECL_T : ECL_NIL;
}
cl_object
ea_to_object(eattr *e)
{
	struct f_val *res = clbird_alloc(sizeof(struct f_val));
	switch (e->type & EAF_TYPE_MASK) {
		case EAF_TYPE_INT:
			return ecl_make_fixnum(e->u.data);
		case EAF_TYPE_ROUTER_ID:
			res->type = T_QUAD;
			res->val.i = e->u.data;
			break;
		case EAF_TYPE_OPAQUE:
			return eclk("OPAQUE");
		case EAF_TYPE_IP_ADDRESS:
			res->type = T_IP;
			res->val.ip = *((ip_addr *) e->u.ptr->data);
			break;
		case EAF_TYPE_AS_PATH:
			res->type = T_PATH;
			res->val.ad = e->u.ptr;
			break;
		case EAF_TYPE_BITFIELD:
			return ecl_make_fixnum(e->u.data);
			break;
		case EAF_TYPE_INT_SET:
			res->type = T_CLIST;
			res->val.ad = e->u.ptr;
			break;
		case EAF_TYPE_EC_SET:
			res->type = T_ECLIST;
			res->val.ad = e->u.ptr;
			break;
		case EAF_TYPE_LC_SET:
			res->type = T_LCLIST;
			res->val.ad = e->u.ptr;
			break;
		case EAF_TYPE_UNDEF:
			return ECL_NIL;
			break;
		default:
			FEerror("failed to translate BIRD dynamic attribute of type ~A", 1, ecl_make_fixnum(e->type));
	}
	return ecl_make_pointer(res);
}
cl_object
clbird_get_eattrs(cl_object rte_obj)
{
	struct rte *rte = ecl_foreign_data_pointer_safe(rte_obj);
	cl_object ret = ECL_NIL;
	for (ea_list *eal = rte->attrs->eattrs; eal; eal=eal->next) {
		for (int i = 0; i < eal->count; i++) {
			eattr *e = &eal->attrs[i];
			cl_object id = ecl_make_fixnum(e->id);
			cl_object attr = ecl_cons(id, ea_to_object(e));
			ret = ecl_cons(attr, ret);
		}
	}
	return ret;
}
cl_object
clbird_get_eattr(cl_object rte_obj, cl_object id_obj)
{
	struct rte *rte = ecl_foreign_data_pointer_safe(rte_obj);
	cl_fixnum id = ecl_to_fix(id_obj);
	for (ea_list *eal = rte->attrs->eattrs; eal; eal=eal->next) {
		for (int i = 0; i < eal->count; i++) {
			eattr *e = &eal->attrs[i];
			if (id == e->id) {
				return ea_to_object(e);
			}
		}
	}
	return ECL_NIL;
}

void
init_ecl_functions()
{
	cl_object package = ecl_find_package("BIRD-IMPL");
	ecl_def_c_function(_ecl_intern("VERSION", package), clbird_version, 0);
	ecl_def_c_function(_ecl_intern("HOSTNAME", package), clbird_hostname, 0);
	ecl_def_c_function(_ecl_intern("ROUTER-ID", package), clbird_router_id, 0);
	ecl_def_c_function(_ecl_intern("NULLPTR", package), clbird_nullptr, 0);
	ecl_def_c_function(_ecl_intern("FORMAT-VAL", package), clbird_format_val, 1);
	ecl_def_c_function(_ecl_intern("GET-VAL", package), clbird_get_val, 2);
	ecl_def_c_function(_ecl_intern("ROUTING-TABLES", package), clbird_get_routing_tables, 0);
	ecl_def_c_function(_ecl_intern("TABLE-NAME", package), clbird_get_table_name, 1);
	ecl_def_c_function(_ecl_intern("TABLE-ROUTES", package), clbird_get_table_routes_all, 1);
	ecl_def_c_function(_ecl_intern("FOR-TABLE-ROUTES", package), clbird_for_table_routes_all, 2);
	ecl_def_c_function(_ecl_intern("TABLE-ROUTES-NET", package), clbird_get_table_routes_net, 2);
	ecl_def_c_function(_ecl_intern("CIDR4", package), clbird_cidr4, 5);
	ecl_def_c_function(_ecl_intern("UNCIDR4", package), clbird_uncidr4, 1);
	ecl_def_c_function(_ecl_intern("MAKE-QUAD", package), clbird_make_quad, 1);
	ecl_def_c_function(_ecl_intern("UNMAKE-QUAD", package), clbird_unmake_quad, 1);
	ecl_def_c_function(_ecl_intern("MAKE-PAIR", package), clbird_make_pair, 2);
	ecl_def_c_function(_ecl_intern("UNMAKE-PAIR", package), clbird_unmake_pair, 1);
	ecl_def_c_function(_ecl_intern("ROUTE-EATTRS", package), clbird_get_eattrs, 1);
	ecl_def_c_function(_ecl_intern("ROUTE-EATTR", package), clbird_get_eattr, 2);
	ecl_def_c_function(_ecl_intern("VAL-TYPE-NAME", package), clbird_val_type_name, 1);
	ecl_def_c_function(_ecl_intern("VAL-TYPE-ID", package), clbird_val_type_id, 1);
	ecl_def_c_function(_ecl_intern("VAL-COMPARE", package), clbird_val_compare, 2);
	ecl_def_c_function(_ecl_intern("VAL-SAME", package), clbird_val_same, 2);
	ecl_def_c_function(_ecl_intern("VAL-IN-RANGE", package), clbird_val_in_range, 2);
}

void
clbird_init(int argc, char **argv)
{
  ecl_set_option(ECL_OPT_TRAP_SIGSEGV, FALSE);
  ecl_set_option(ECL_OPT_TRAP_SIGFPE, FALSE);
  ecl_set_option(ECL_OPT_TRAP_SIGINT, FALSE);
  ecl_set_option(ECL_OPT_TRAP_SIGILL, FALSE);
  ecl_set_option(ECL_OPT_TRAP_INTERRUPT_SIGNAL, FALSE);
  cl_boot(argc, argv);
  ecl_init_module(NULL, clbird_native_lisp_init);
  init_ecl_functions();
}

char*
ecl_get_string_for(cl_object obj) {
	cl_object formatted = NULL;
	ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ECL_T)) {
		formatted = cl_princ_to_string(obj);
	} ECL_HANDLER_CASE(1, condition) {
	} ECL_HANDLER_CASE_END;
	if (formatted == NULL) {
		return "[error occurred printing Lisp object]";
	}
	assert(ECL_STRINGP(formatted));
	if (!ecl_fits_in_base_string(formatted)) {
		return "[Unicode error printing Lisp object]";
	}
	cl_object base = si_coerce_to_base_string(formatted);
	char* ret = base->base_string.self;
	return ret;
}

void
clbird_load_and_byte_compile(char *file)
{
	static cl_object bc_compile_file = NULL;
	if (bc_compile_file == NULL) {
		cl_object compiler_pkg = ecl_find_package("EXT");
		assert(compiler_pkg != ECL_NIL);
		cl_object sym = _ecl_intern("COMPILE-FILE", compiler_pkg);
		bc_compile_file = cl_symbol_function(sym);
		assert(bc_compile_file != ECL_NIL);
	}
	ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ecl_make_symbol("ERROR", "CL"))) {
		log(L_INFO "Byte-compiling and loading Lisp: %s", file);
		cl_funcall(4, bc_compile_file, ecls(file), eclk("LOAD"), ECL_T);
		log(L_INFO "Lisp successfully loaded");
	} ECL_HANDLER_CASE(1, condition) {
		char* cond_fmt = ecl_get_string_for(condition);
		log(L_WARN "Failed to byte-compile %s: %s", file, cond_fmt);
	} ECL_HANDLER_CASE_END;
}

void
clbird_load(char *file)
{
	ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ECL_T)) {
		log(L_INFO "Loading Lisp: %s", file);
		cl_load(1, ecls(file));
		log(L_INFO "Lisp successfully loaded");
	} ECL_HANDLER_CASE(1, condition) {
		char* cond_fmt = ecl_get_string_for(condition);
		log(L_WARN "Failed to load %s: %s", file, cond_fmt);
	} ECL_HANDLER_CASE_END;
}
